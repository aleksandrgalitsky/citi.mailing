﻿namespace CITI.Mailing.Blob
{
    public class BlobFileProperties
    {
        public long Length { get; set; }

        public string MD5 { get; set; }
    }
}