﻿using CITI.Mailing.Blob.Contracts;
using CITI.Mailing.Blob.Contracts.Exceptions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CITI.Mailing.Blob
{
    public class AzureBlobClient : IAzureBlobClient
    {
        private CloudBlobClient _blobClient;
        
        public AzureBlobClient(BlobStorageConnectionConfiguration connectionConfiguration)
        {
            var storageAccount = CloudStorageAccount.Parse(connectionConfiguration.GetConnectionString());
            _blobClient = storageAccount.CreateCloudBlobClient();
            _blobClient.DefaultRequestOptions.StoreBlobContentMD5 = connectionConfiguration.StoreBlobContentMD5;
        }
        
        public async Task<IEnumerable<string>> ListDirectiries(string container, string path = null)
        {
            var results = await ListSegments(container, path);
            return results.OfType<CloudBlobDirectory>().Select(x => x.Prefix).ToList();
        }
        
        public async Task<IEnumerable<string>> ListFiles(string container, string path)
        {
            var results = await ListSegments(container, path);
            return results.OfType<CloudBlockBlob>().Select(x => x.Name).ToList()
                .Union(results.OfType<CloudAppendBlob>().Select(x => x.Name).ToList());
        }
        
        public async Task<bool> FileExists(string container, string path)
        {
            var blobContainer = await GetContainer(container);
            var blob = blobContainer.GetBlobReference(path);
            return await blob.ExistsAsync();
        }
        
        public async Task<BlobFileProperties> GetProperties(string container, string path)
        {
            var blobContainer = await GetContainer(container);
            var blob = blobContainer.GetBlobReference(path);
            await blob.FetchAttributesAsync();
            
            return new BlobFileProperties
            {
                Length = blob.Properties.Length,
                MD5 = blob.Properties.ContentMD5
            };
        }
        
        public async Task Delete(string container, string path)
        {
            var blobContainer = await GetContainer(container);
            var blob = blobContainer.GetBlobReference(path);
            await blob.DeleteAsync();
        }
        
        public async Task DeleteIfExists(string container, string path)
        {
            var blobContainer = await GetContainer(container);
            var blob = blobContainer.GetBlobReference(path);
            await blob.DeleteIfExistsAsync();
        }
        
        public async Task UploadFile(string containerName, string fileName, Stream fileContent)
        {
            fileContent.Position = 0;
            var container = await GetContainer(containerName);
            await container.CreateIfNotExistsAsync();
            container.GetBlockBlobReference(fileName);
            var block = container.GetBlockBlobReference(fileName);
            await block.UploadFromStreamAsync(fileContent);
        }
        
        public async Task UploadBlock(string containerName, string fileName, Stream blockStream, string blockId)
        {
            var container = await GetContainer(containerName);
            container.GetBlockBlobReference(fileName);
            var block = container.GetBlockBlobReference(fileName);
            
            var md5 = GetMD5HashFromStream(blockStream);
            
            blockStream.Position = 0;
            await block.PutBlockAsync(blockId, blockStream, md5);
        }
        
        public async Task CommitBlocks(string containerName, string fileName, IEnumerable<string> blocks)
        {
            var container = await GetContainer(containerName);
            container.GetBlockBlobReference(fileName);
            var block = container.GetBlockBlobReference(fileName);
            await block.PutBlockListAsync(blocks);
        }
        
        public async Task<Stream> DownloadFile(string containerName, string fileName)
        {
            var block = await GetFileBlock(containerName, fileName);
            var stream = new MemoryStream();
            await block.DownloadToStreamAsync(stream);
            stream.Position = 0;
            return stream;
        }

        public async Task<Stream> DownloadFileChunk(string containerName, string fileName, long? offset, long? length)
        {
            var block = await GetFileBlock(containerName, fileName);
            var stream = new MemoryStream();
            await block.DownloadRangeToStreamAsync(stream, offset, length);
            stream.Position = 0;
            return stream;
        }
        
        public async Task<string> AcquireLease(string containerName, string file, TimeSpan? breakTimeout = null)
        {
            var breakTimeoutValue = breakTimeout ?? TimeSpan.FromDays(1);
            
            var container = await GetContainer(containerName);
            var block = container.GetBlockBlobReference(file);
            var blockExists = await block.ExistsAsync();
            
            if (!blockExists)
            {
                try
                {
                    await block.UploadFromStreamAsync(new MemoryStream());
                }
                catch (StorageException ex)
                {
                    if (ex.Message.Equals("The remote server returned an error: (412) There is currently a lease on the blob and no lease ID was specified in the request..")
                        || ex.Message.Equals("The remote server returned an error: (409) Conflict."))
                    {
                        throw new LeaseConflictException();
                    }
                }
            }
            
            try
            {
                if (block.Properties.LeaseStatus == LeaseStatus.Locked)
                {
                    if (block.Properties.LastModified.HasValue && block.Properties.LastModified.Value.Add(breakTimeoutValue) < DateTime.UtcNow)
                    {
                        await block.BreakLeaseAsync(null);
                    }
                    else
                    {
                        throw new LeaseConflictException();
                    }
                }
                
                var leaseId = await block.AcquireLeaseAsync(null);
                var accessCondition = AccessCondition.GenerateLeaseCondition(leaseId);
                await block.UploadFromStreamAsync(new MemoryStream(), accessCondition, null, null);
                
                return leaseId;
            }
            catch (StorageException ex)
            {
                if (ex.Message == "The remote server returned an error: (409) Conflict.")
                {
                    throw new LeaseConflictException();
                }
                
                throw;
            }
        }
        
        public async Task ReleaseLease(string container, string file, string leaseId)
        {
            var block = await GetFileBlock(container, file);
            
            try
            {
                var accessCondition = AccessCondition.GenerateLeaseCondition(leaseId);
                await block.DeleteIfExistsAsync(DeleteSnapshotsOption.None, accessCondition, null, null);
            }
            catch
            {
                await block.BreakLeaseAsync(null);
            }
        }
        
        public async Task AppendText(string container, string filePath, string content)
        {
            var block = await GetAppendBlock(container, filePath);
            await block.AppendTextAsync(content);
        }
        
        #region private methods
        
        private async Task<IEnumerable<IListBlobItem>> ListSegments(string container, string path = null)
        {
            var blobContainer = await GetContainer(container);
            var results = new List<IListBlobItem>();
            
            if (!(await blobContainer.ExistsAsync()))
            {
                return results;
            }
            
            BlobContinuationToken continuationToken = null;

            if (path == null)
            {
                do
                {
                    var response = await blobContainer.ListBlobsSegmentedAsync(continuationToken);
                    continuationToken = response.ContinuationToken;
                    results.AddRange(response.Results);
                }
                while (continuationToken != null);
            }
            else
            {
                var directory = blobContainer.GetDirectoryReference(path);
                
                do
                {
                    var response = await directory.ListBlobsSegmentedAsync(continuationToken);
                    continuationToken = response.ContinuationToken;
                    results.AddRange(response.Results);
                }
                while (continuationToken != null);
            }
            
            return results;
        }
        
        private async Task<CloudBlob> GetFileBlock(string containerName, string fileName)
        {
            var container = await GetContainer(containerName);
            return container.GetBlobReference(fileName);
        }
        
        private async Task<CloudAppendBlob> GetAppendBlock(string containerName, string fileName)
        {
            var container = await GetContainer(containerName);
            var block = container.GetAppendBlobReference(fileName);
            var isExists = await block.ExistsAsync();
            
            if (!isExists)
            {
                await block.CreateOrReplaceAsync();
            }
            
            return block;
        }
        
        private async Task<CloudBlobContainer> GetContainer(string containerName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new ArgumentException("ContainerName is missing");
            }
            
            var blobContainer = _blobClient.GetContainerReference(containerName.ToLower());
            return await Task.FromResult(blobContainer);
        }
        
        private static string GetMD5HashFromStream(Stream stream)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] blockHash = md5.ComputeHash(stream);
            return Convert.ToBase64String(blockHash, 0, 16);
        }
        
        #endregion private methods
    }
}
