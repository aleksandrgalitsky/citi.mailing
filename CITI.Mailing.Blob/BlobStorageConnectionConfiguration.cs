﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Blob
{
    public class BlobStorageConnectionConfiguration
    {
        private string _connectionString;
        public bool StoreBlobContentMD5;

        public BlobStorageConnectionConfiguration(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string GetConnectionString()
        {
            return _connectionString;
        }
    }
}
