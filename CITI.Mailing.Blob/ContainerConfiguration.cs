﻿using CITI.Mailing.Blob.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Blob
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            servicesCollection.AddSingleton(new BlobStorageConnectionConfiguration(config.GetConnectionString("QueueStorageConnection")));

            servicesCollection.AddScoped<IAzureBlobClient, AzureBlobClient>();
        }
    }
}
