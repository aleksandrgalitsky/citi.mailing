﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Blob.Contracts
{
    public interface IAzureBlobClient
    {
        Task<IEnumerable<string>> ListDirectiries(string container, string path = null);
        
        Task<IEnumerable<string>> ListFiles(string container, string path);

        Task<bool> FileExists(string container, string path);

        Task<BlobFileProperties> GetProperties(string container, string path);

        Task Delete(string container, string path);

        Task DeleteIfExists(string container, string path);

        Task UploadFile(string containerName, string fileName, Stream fileContent);

        Task UploadBlock(string containerName, string fileName, Stream blockStream, string blockId);

        Task CommitBlocks(string containerName, string fileName, IEnumerable<string> blocks);

        Task<Stream> DownloadFile(string containerName, string fileName);

        Task<Stream> DownloadFileChunk(string containerName, string fileName, long? offset, long? length);

        Task<string> AcquireLease(string containerName, string file, TimeSpan? breakTimeout = null);

        Task ReleaseLease(string container, string file, string leaseId);
        
        Task AppendText(string container, string filePath, string content);
    }
}
