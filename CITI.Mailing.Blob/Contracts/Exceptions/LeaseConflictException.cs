﻿using System;
using System.Runtime.Serialization;

namespace CITI.Mailing.Blob.Contracts.Exceptions
{
    [Serializable]
    internal class LeaseConflictException : Exception
    {
        public LeaseConflictException()
        {
        }

        public LeaseConflictException(string message) : base(message)
        {
        }

        public LeaseConflictException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LeaseConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}