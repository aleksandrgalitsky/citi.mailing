﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Communication
{
    public class StorageConnectionConfiguration
    {
        private string _connectionString;

        public StorageConnectionConfiguration(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string GetConnectionString()
        {
            return _connectionString;
        }
    }
}
