﻿using CITI.Mailing.Communication.Contracts;
using CITI.Mailing.Communication.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace CITI.Mailing.Communication
{
    public class QueueManager : IQueueManager
    {
        private CloudQueue _queue;
        private CloudQueueClient _queueClient;
        private CloudStorageAccount _storageAccount;
        public const string QUEUE_NAME = "email-queue";

        public QueueManager(StorageConnectionConfiguration connectionConfiguration)
        {
            _storageAccount = CloudStorageAccount.Parse(connectionConfiguration.GetConnectionString());
            _queueClient = _storageAccount.CreateCloudQueueClient();
            _queue = _queueClient.GetQueueReference(QUEUE_NAME);
        }

        public async Task Enqueue(Message message, DateTime? schedule)
        {
            await EnqueueMessageAsync(message, schedule);
        }

        protected async Task EnqueueMessageAsync(Message message, DateTime? schedule)
        {
            if (schedule.HasValue)
            {
                try
                {
                    await _queue.AddMessageAsync(new CloudQueueMessage(JsonConvert.SerializeObject(message)), null,
                    schedule.Value.Subtract(DateTime.UtcNow), new QueueRequestOptions(), new OperationContext());
                }
                catch(Exception excep)
                {
                    Console.WriteLine(excep.Message);
                }
            }
            else
            {
                await _queue.AddMessageAsync(new CloudQueueMessage(JsonConvert.SerializeObject(message)));
            }
        }

        public async Task EnusureQueueCreated()
        {
            await _queue.CreateIfNotExistsAsync();
        }
    }
}
