﻿using CITI.Mailing.Communication.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Communication
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            servicesCollection.AddSingleton(new StorageConnectionConfiguration(config.GetConnectionString("QueueStorageConnection")));
            
            servicesCollection.AddScoped<IQueueManager, QueueManager>();
        }
    }
}
