﻿using CITI.Mailing.Communication.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Communication.Contracts
{
    public interface IQueueManager
    {
        Task Enqueue(Message message, DateTime? schedule = null);

        Task EnusureQueueCreated();
    }
}
