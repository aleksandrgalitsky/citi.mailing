﻿using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;
using CITI.Mailing.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Data
{
    public static class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            servicesCollection.AddSingleton(new MailingDataConnectionConfiguration(config.GetConnectionString("CosmosDb"), config["CosmosDbName"]));

            servicesCollection.AddScoped<MailingDataContext>();

            servicesCollection.AddScoped<IMailingDataContextManager, MailingDataContextManager>();

            servicesCollection.AddScoped<ITemplateRepository, TemplateRepository>();
            servicesCollection.AddScoped<IMessageRepository, MessageRepository>();
            servicesCollection.AddScoped<ITemplateBodyRepository, TemplateBodyRepository>();
        }
    }
}
