﻿using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;
using CITI.Mailing.Data.Repositories;
using CITI.Mailing.Model;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data
{
    public class MailingDataContextManager : IMailingDataContextManager
    {
        private MailingDataConnectionConfiguration _connectionConfiguration;
        private IServiceProvider _servicePrivider;
        private MailingDataContext _context;
        private static bool _isInitialized;

        public MailingDataContextManager(MailingDataConnectionConfiguration connectionConfiguration, IServiceProvider serviceProvider)
        {
            _connectionConfiguration = connectionConfiguration;
            _servicePrivider = serviceProvider;
        }

        public T CreateRepository<T>()
            where T : IRepository
        {
            if (_context == null)
            {
                _context = new MailingDataContext(_connectionConfiguration);
            }

            var repo = _servicePrivider.GetRequiredService<T>();
            (repo as Repository).SetContext(_context);
            return repo;
        }

        public async Task Save()
        {
            if (_context == null)
            {
                _context = new MailingDataContext(_connectionConfiguration);
            }

            await _context.SaveChanges();
        }

        public async Task EnsureInitialized()
        {
            if (!_isInitialized)
            {
                await Context.EnsureContainer(typeof(Template).Name);
                await Context.EnsureContainer(typeof(TemplateBody).Name);
                await Context.EnsureContainer(typeof(Message).Name);
            }

            _isInitialized = true;
        }

        private MailingDataContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new MailingDataContext(_connectionConfiguration);
                }
                return _context;
            }
        }
    }
}
