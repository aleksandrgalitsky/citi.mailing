﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Data
{
    public class MailingDataConnectionConfiguration
    {
        public MailingDataConnectionConfiguration(string connectionString, string databaseId)
        {
            ConnectionString = connectionString;
            DatabaseId = databaseId;
        }

        public string ConnectionString { get; }
        public string DatabaseId { get; }
    }
}
