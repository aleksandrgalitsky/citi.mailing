﻿using CITI.Mailing.Data.Contracts.Repositories;
using CITI.Mailing.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Contracts
{
    public interface IMailingDataContextManager
    {
        T CreateRepository<T>() where T : IRepository;

        Task Save();
    }
}
