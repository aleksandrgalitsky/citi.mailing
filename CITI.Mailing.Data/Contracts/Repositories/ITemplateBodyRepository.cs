﻿using CITI.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Contracts.Repositories
{
    public interface ITemplateBodyRepository : IRepository
    {
        Task<IEnumerable<TemplateBody>> GetAll();

        Task<TemplateBody> Get(string id);

        void AddOrUpdate(TemplateBody template);

        void Delete(TemplateBody template);

        Task<IEnumerable<TemplateBody>> GetQuery(Expression<Func<TemplateBody, bool>> expression = null);
    }
}
