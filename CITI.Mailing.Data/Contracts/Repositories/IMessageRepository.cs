﻿using CITI.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Contracts.Repositories
{
    public interface IMessageRepository : IRepository
    {
        Task<IEnumerable<Message>> GetAll();

        Task<Message> Get(string id);

        void AddOrUpdate(Message template);

        void Delete(Message template);

        Task<IEnumerable<Message>> GetQuery(Expression<Func<Message, bool>> expression = null);
    }
}
