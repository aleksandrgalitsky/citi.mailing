﻿using CITI.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Contracts.Repositories
{
    public interface ITemplateRepository : IRepository
    {
        Task<IEnumerable<Template>> GetAll();

        Task<Template> Get(string id);

        void AddOrUpdate(Template template);

        void Delete(Template template);

        Task<IEnumerable<Template>> GetQuery(Expression<Func<Template, bool>> expression = null);
    }
}
