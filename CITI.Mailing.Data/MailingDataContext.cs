﻿using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CITI.Mailing.Data
{
    public class MailingDataContext
    {
        private MailingDataConnectionConfiguration _connectionConfiguration;
        private CosmosClient _cosmosClient;
        private Database _database;
        private List<Func<Task>> _commands;

        public MailingDataContext(MailingDataConnectionConfiguration connectionConfiguration)
        {
            _connectionConfiguration = connectionConfiguration;
            _commands = new List<Func<Task>>();
        }

        public async Task EnsureDb()
        {
            //await _cosmosClient.CreateDatabaseIfNotExistsAsync(_connectionConfiguration.DatabaseId);
            await Client.CreateDatabaseIfNotExistsAsync(_connectionConfiguration.DatabaseId);
        }

        internal CosmosClient Client
        {
            get
            {
                if (_cosmosClient == null)
                {
                    _cosmosClient = new CosmosClient(_connectionConfiguration.ConnectionString);
                }
                return _cosmosClient;
            }
        }

        internal Database Database
        {
            get
            {
                if (_database == null)
                {
                    _database = Client.GetDatabase(_connectionConfiguration.DatabaseId);
                }

                return _database;
            }
        }

        internal Container GetContainer(string containerId)
        {
            return Database.GetContainer(containerId);
        }

        internal async Task<Container> EnsureContainer(string containerId)
        {
            return await Database.CreateContainerIfNotExistsAsync(containerId, "/id");
        }

        internal void AddCommand(Func<Task> task)
        {
            _commands.Add(task);
        }

        public async Task<int> SaveChanges()
        {
            await EnsureDb();
            await EnsureContainer("Template");
            await EnsureContainer("TemplateBody");
            await EnsureContainer("Message");

            var qty = _commands.Count;

            foreach (var command in _commands)
            {
                try
                {
                    await command();
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }
            }

            _commands.Clear();
            return qty;
        }
    }
}
