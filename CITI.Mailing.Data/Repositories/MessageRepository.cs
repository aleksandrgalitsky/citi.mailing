﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Azure.Cosmos.Linq;
using System.Threading.Tasks;
using CITI.Mailing.Model;
using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;
namespace CITI.Mailing.Data.Repositories
{
    public class MessageRepository : Repository, IMessageRepository
    {
        public async Task<IEnumerable<Message>> GetAll()
        {
            return await GetQuery<Message>();
        }

        public async Task<Message> Get(string id)
        {
            return await GetByIdAsync<Message>(id);
        }

        public void AddOrUpdate(Message message)
        {
            base.AddOrUpdate(message);
        }

        public void Delete(Message message)
        {
            base.Delete<Message>(message.Id);
        }

        public Task<IEnumerable<Message>> GetQuery(Expression<Func<Message, bool>> expression = null)
        {
            return base.GetQuery<Message>(expression);
        }
    }
}
