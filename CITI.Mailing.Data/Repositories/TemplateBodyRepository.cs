﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Azure.Cosmos.Linq;
using System.Threading.Tasks;
using CITI.Mailing.Model;
using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;

namespace CITI.Mailing.Data.Repositories
{
    public class TemplateBodyRepository : Repository, ITemplateBodyRepository
    {
        public async Task<IEnumerable<TemplateBody>> GetAll()
        {
            return await GetQuery<TemplateBody>();
        }

        public async Task<TemplateBody> Get(string id)
        {
            return await GetByIdAsync<TemplateBody>(id);
        }

        public void AddOrUpdate(TemplateBody template)
        {
            base.AddOrUpdate(template);
        }

        public void Delete(TemplateBody template)
        {
            base.Delete<TemplateBody>(template.Id);
        }

        public Task<IEnumerable<TemplateBody>> GetQuery(Expression<Func<TemplateBody, bool>> expression = null)
        {
            return base.GetQuery<TemplateBody>(expression);
        }
    }
}
