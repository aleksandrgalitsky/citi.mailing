﻿using CITI.Mailing.Data.Contracts.Repositories;
using CITI.Mailing.Model.Contracts;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Repositories
{
    public abstract class Repository : IRepository
    {
        private MailingDataContext _context;

        internal void SetContext(MailingDataContext context)
        {
            _context = context;
        }

        protected async Task<T> GetByIdAsync<T>(string id)
        {
            return await this.GetByIdAsync<T>(typeof(T).Name, id);
        }

        protected async Task<T> GetByIdAsync<T>(string containerName, string id)
        {
            return await _context.GetContainer(containerName).ReadItemAsync<T>(id, ResolvePartitionKey(id));
        }

        protected async Task<IEnumerable<T>> GetQuery<T>(string containerName, Expression<Func<T, bool>> expression)
        {
            IQueryable<T> query = _context.GetContainer(containerName).GetItemLinqQueryable<T>();

            if (expression != null)
            {
                query = query.Where(expression);
            }

            var iterator = query.ToFeedIterator();
            return await iterator.ReadNextAsync();
        }

        protected async Task<IEnumerable<T>> GetQuery<T>(Expression<Func<T, bool>> expression = null)
        {
            return await GetQuery(typeof(T).Name, expression);
        }

        protected void AddOrUpdate<T>(string containerName, T document)
            where T : IEntity
        {
            _context.AddCommand(() => _context.GetContainer(containerName).UpsertItemAsync(document, ResolvePartitionKey(document.Id)));
        }

        protected void AddOrUpdate<T>(T document)
            where T : IEntity
        {
            this.AddOrUpdate<T>(typeof(T).Name, document);
        }

        protected void Delete<T>(string containerName, string id)
            where T : IEntity
        {
            _context.AddCommand(() => _context.GetContainer(containerName).DeleteItemAsync<T>(id, ResolvePartitionKey(id)));
        }

        protected void Delete<T>(string id)
            where T : IEntity
        {
            this.Delete<T>(typeof(T).Name, id);
        }

        protected virtual PartitionKey ResolvePartitionKey(string entityId) => new PartitionKey(entityId);
    }
}
