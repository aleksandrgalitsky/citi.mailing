﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Azure.Cosmos.Linq;
using System.Threading.Tasks;
using CITI.Mailing.Model;
using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;

namespace CITI.Mailing.Data.Repositories
{
    public class TemplateRepository : Repository, ITemplateRepository
    {
        public async Task<IEnumerable<Template>> GetAll()
        {
            return await GetQuery<Template>();
        }

        public async Task<Template> Get(string id)
        {
            return await GetByIdAsync<Template>(id);
        }

        public void AddOrUpdate(Template template)
        {
            base.AddOrUpdate(template);
        }

        public void Delete(Template template)
        {
            base.Delete<Template>(template.Id);
        }

        public Task<IEnumerable<Template>> GetQuery(Expression<Func<Template, bool>> expression = null)
        {
            return base.GetQuery<Template>(expression);
        }
    }
}
