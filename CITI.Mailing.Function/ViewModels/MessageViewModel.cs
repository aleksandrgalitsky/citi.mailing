﻿using System;
using System.Collections.Generic;
using System.Text;
using CITI.Mailing.Function.Models;

namespace CITI.Mailing.Function.ViewModels
{
    public class MessageViewModel
    {
        public string Id { get; set; }

        public List<RecepientModel> Recepients { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public DateTime Created { get; set; }

        public DateTime ScheduledAt { get; set; }

        public bool IsActive { get; set; }

        public bool IsSent { get; set; }

        public string TemplateId { get; set; }
    }
}
