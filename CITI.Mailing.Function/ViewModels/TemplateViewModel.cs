﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.ViewModels
{
    public class TemplateViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public bool IsActive { get; set; }
    }
}
