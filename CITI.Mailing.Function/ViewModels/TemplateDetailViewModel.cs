﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.ViewModels
{
    public class TemplateDetailViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public string Language { get; set; }

        public bool IsActive { get; set; }

        public string Body { get; set; }
    }
}
