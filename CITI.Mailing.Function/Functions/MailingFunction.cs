﻿using CITI.Mailing.Function.Contracts;
using CITI.Mailing.Function.Models;
using CITI.Mailing.Function.ViewModels;
using CITI.Mailing.Model;
using CITI.Mailing.Services;
using CITI.Mailing.Services.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Function.Functions
{
    public class MailingFunction : IMailingFunction
    {
        private IMailingService _mailingService;

        public ILogger Log { get; set; }

        public MailingFunction(IMailingService mailingService)
        {
            _mailingService = mailingService;
        }

        public async Task<List<TemplateFull>> GetAll()
        {
            var data = await _mailingService.GetAll();

            return data;
        }

        public async Task<TemplateFull> Get(string id)
        {
            var data = await _mailingService.Get(id);
            var bodies = await _mailingService.GetTemplateBody(data.App, data.Event);

            var model = new TemplateFull
            {
                Id = data.Id,
                Name = data.Name,
                Event = data.Event,
                IsActive = data.IsActive,
                App = data.App,
                Items = new List<TemplateFullBody>()
            };

            foreach(var body in bodies)
            {
                TemplateFullBody b = new TemplateFullBody()
                {
                    Id = body.Id,
                    Body = await _mailingService.GetBlob(id, body.Language),
                    Language = body.Language
                };

                model.Items.Add(b);
            }
            
            return model;
        }

        public async Task<string> GetBlob(string id, string language)
        {
            return await _mailingService.GetBlob(id, language);
        }

        public async Task<TemplateViewModel> Create(TemplateModel template)
        {
            TemplateFull tpl = new TemplateFull()
            {
                Name = template.Name,
                App = template.App,
                Event = template.Event,
                IsActive = true,
                Items = new List<TemplateFullBody>()
            };

            foreach(var body in template.Items)
            {
                TemplateFullBody b = new TemplateFullBody();
                b.Language = body.Language;
                b.Body = body.Body;
                tpl.Items.Add(b);
            }

            var data = await _mailingService.Create(tpl);

            TemplateViewModel result = new TemplateViewModel()
            {
                Id = data.Id,
                App = data.App,
                Event = data.Event,
                IsActive = data.IsActive,
                Name = data.Name
            };
            
            return result;
        }

        public async Task<TemplateFull> Update(TemplateFull template)
        {
            var data = await _mailingService.Update(template);

            return data;
        }

        public async Task Delete(string id)
        {
            await _mailingService.Delete(id);
        }

        public async Task NewQueue()
        {
            await _mailingService.NewQueue();
        }

        public async Task CreateMessage(MessageModel messageModel)
        {
            Recepient recepient = new Recepient()
            {
                Name = messageModel.Recepient.Name,
                Email = messageModel.Recepient.Email
            };

            var template = await _mailingService.Get(messageModel.App, messageModel.Event);

            Message message = new Message()
            {
                Id = Guid.NewGuid().ToString(),
                Recepient = recepient,
                Subject = messageModel.Subject,
                Body = messageModel.Body,
                SenderName = messageModel.SenderName,
                SenderEmail = messageModel.SenderEmail,
                Created = messageModel.Created,
                ScheduledAt = messageModel.ScheduledAt,
                IsActive = messageModel.IsActive,
                IsSent = messageModel.IsSent,
                TemplateId = template.Id,
                Language = messageModel.Language
            };

            await _mailingService.CreateMessage(message, null);
        }

        public async Task Send(string queueMessageId)
        {
            Log.LogInformation($"TemplateFunction.Send started. Message ID: {queueMessageId}");
            await _mailingService.Send(queueMessageId);
            Log.LogInformation($"TemplateFunction.Send completed. Message ID: {queueMessageId}");
        }

        public async Task<List<MessageModel>> GetMessages(string templateId, DateTime fromDate, DateTime toDate, int status)
        {
            return new List<MessageModel>();
        }
    }
}
