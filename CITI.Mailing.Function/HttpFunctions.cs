using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CITI.Mailing.Function.Extensions;
using CITI.Mailing.Function.Contracts;
using CITI.Mailing.Data;
using CITI.Mailing.Function.ViewModels;
using CITI.Mailing.Function.Models;
using RazorEngine;
using RazorEngine.Templating;
using Newtonsoft.Json.Linq;
using CITI.Mailing.Blob;
using RazorEngine.Configuration;
using RazorEngine.Text;
using CITI.Mailing.Communication;
using CITI.Mailing.Communication.Models;
using System.Collections.Generic;
using System.Net.Http;
using CITI.Mailing.Services;

namespace CITI.Mailing.Function
{
    public static class HttpFunctions
    {
        [FunctionName("GetAllTemplates")]
        public static async Task<IActionResult> GetAllTemplates([HttpTrigger(AuthorizationLevel.Function, "get", Route = "template/list")] HttpRequest req,
            ExecutionContext context, ILogger log)
        {
            var function = context.GetFunction<IMailingFunction>(log);
            var model = await function.GetAll();
            return new OkObjectResult(model);
        }

        [FunctionName("GetTemplate")]
        public static async Task<IActionResult> GetTemplate([HttpTrigger(AuthorizationLevel.Function, "get", Route = "template/{id}")] HttpRequest req,
            string id, ExecutionContext context, ILogger log)
        {
            var function = context.GetFunction<IMailingFunction>(log);
            var model = await function.Get(id);
            
            return new OkObjectResult(model);
        }
        

        [FunctionName("CreateTemplate")]
        public static async Task<IActionResult> CreateTemplate([HttpTrigger(AuthorizationLevel.Function, "post", Route = "template/")]
            HttpRequest req, ExecutionContext context, ILogger log)
        {
            
            var function = context.GetFunction<IMailingFunction>(log);

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            TemplateModel template = JsonConvert.DeserializeObject<TemplateModel>(requestBody);

            var model = await function.Create(template);
            return new OkObjectResult(model);
        }
        
        [FunctionName("UpdateTemplate")]
        public static async Task<IActionResult> UpdateTemplate([HttpTrigger(AuthorizationLevel.Function, "put", Route = "template/")]
            HttpRequest req, ExecutionContext context, ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            TemplateFull template = JsonConvert.DeserializeObject<TemplateFull>(requestBody);

            var function = context.GetFunction<IMailingFunction>(log);
            var model = await function.Update(template);
            return new OkObjectResult(new { Status = "ok" });
        }
        
        [FunctionName("DeleteTemplate")]
        public static async Task<IActionResult> DeleteTemplate([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "template/{id}")] HttpRequest req,
            string id, ExecutionContext context, ILogger log)
        {
            var function = context.GetFunction<IMailingFunction>(log);
            await function.Delete(id);

            return new OkObjectResult(new { Status = "ok" });
        }

        [FunctionName("GetLanguages")]
        public static async Task<IActionResult> GetLanguages([HttpTrigger(AuthorizationLevel.Function, "get", Route = "template/languages")] HttpRequest req,
            ExecutionContext context, ILogger log)
        {
            var langs = new Dictionary<string, string>();
            langs.Add("FRA", "France");
            langs.Add("ENG", "English");
            langs.Add("DEU", "Germany");
            langs.Add("ITA", "Italy");
            langs.Add("DUT", "Dutch");

            return new OkObjectResult(new { langLookup = langs });
        }

        [FunctionName("Render")]
        public static async Task<IActionResult> Render([HttpTrigger(AuthorizationLevel.Function, "post", Route = "template/render")]
            [FromBody] RenderModel model, ExecutionContext context, ILogger log)
        {
            string template = model.Body.Replace("<!--TPL", "").Replace("TPL-->", "");
            dynamic renderModel = model.Object;

            var config = new TemplateServiceConfiguration();
            config.Language = Language.CSharp;
            config.EncodedStringFactory = new RawStringFactory();
            config.EncodedStringFactory = new HtmlEncodedStringFactory();
            config.CachingProvider = new DefaultCachingProvider();
            var service = RazorEngineService.Create(config);
            Engine.Razor = service;

            var result = Engine.Razor.RunCompile(template, "templateKey", null, renderModel as object);

            return new OkObjectResult(new { Result = result });
        }

        [FunctionName("Send")]
        public static async Task<IActionResult> Send([HttpTrigger(AuthorizationLevel.Function, "post", Route = "send")]
            SendModel model, ExecutionContext context, ILogger log)
        {
            try
            {
                var function = context.GetFunction<IMailingFunction>(log);

                MessageModel message = new MessageModel()
                {
                    App = model.App,
                    Recepient = model.To,
                    Subject = model.Subject,
                    Body = model.Object.ToString(),
                    SenderName = model.From.Name,
                    SenderEmail = model.From.Email,
                    Created = DateTime.UtcNow,
                    ScheduledAt = DateTime.UtcNow,
                    IsActive = true,
                    IsSent = false,
                    Event = model.Event,
                    Language = model.Language
                };

                await function.CreateMessage(message);

                return new OkObjectResult(new { Result = "Ok" });
            }
            catch(Exception exp)
            {
                return new OkObjectResult(new { Result = exp.Message });
            }
        }

        [FunctionName("Queue")]
        public static async Task QueueTest([QueueTrigger(QueueManager.QUEUE_NAME)] Message message,
            ExecutionContext context, ILogger log)
        {
            log.LogInformation($"Queue function processed: {message.Id}");

            var function = context.GetFunction<IMailingFunction>(log);
            await function.Send(message.Id);

        }
    }
}
