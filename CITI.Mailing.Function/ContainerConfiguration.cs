﻿using CITI.Mailing.Function.Contracts;
using CITI.Mailing.Function.Functions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            Services.ContainerConfiguration.RegisterTypes(servicesCollection, config);

            servicesCollection.AddTransient<IMailingFunction, MailingFunction>();
        }
    }
}
