﻿using CITI.Mailing.Function.Models;
using CITI.Mailing.Function.ViewModels;
using CITI.Mailing.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Function.Contracts
{
    public interface IMailingFunction : IFunction
    {
        Task<TemplateFull> Get(string id);

        Task<string> GetBlob(string id, string language);

        Task<List<TemplateFull>> GetAll();

        Task<TemplateViewModel> Create(TemplateModel model);

        Task<TemplateFull> Update(TemplateFull template);

        Task Delete(string id);

        Task NewQueue();

        Task CreateMessage(MessageModel messageModel);

        Task Send(string queueMessageId);

        Task<List<MessageModel>> GetMessages(string templateId, DateTime fromDate, DateTime toDate, int status);
    }
}
