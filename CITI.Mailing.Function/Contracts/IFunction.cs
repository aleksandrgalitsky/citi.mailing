﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Contracts
{
    public interface IFunction
    {
        ILogger Log { get; set; }
    }
}
