﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public enum MessageStatus
    {
        Sent = 0,
        Scheduled = 1
    }

    public class MessagesFilterModel
    {
        public string TemplateId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public MessageStatus Status { get; set; }
    }
}
