﻿using CITI.Mailing.Function.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class Test2Model
    {
        public List<string> Items { get; set; }
    }

    public class TestModel
    {
        public List<string> Items { get; set; }
    }
}
