﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class TemplateBodyModel
    {
        public string Id { get; set; }

        public string Language { get; set; }

        public string Body { get; set; }
    }

    public class TemplateModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public TemplateBodyModel[] Items { get; set; }
    }
}
