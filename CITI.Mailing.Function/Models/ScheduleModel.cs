﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class ScheduleModel
    {
        public SenderModel From { get; set; }

        public RecepientModel To { get; set; }

        public string TemplateId { get; set; }

        public string Subject { get; set; }

        public string Data { get; set; }

        public DateTime ScheduledAt { get; set; }
    }
}
