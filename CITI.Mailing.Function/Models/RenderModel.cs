﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class RenderModel
    {
        public string Body { get; set; }

        public JObject Object { get; set; }
    }
}
