﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class SendModel
    {
        public string App { get; set; }

        public string Language { get; set; }

        public string Event { get; set; }

        public SenderModel From { get; set; }

        public RecepientModel To { get; set; }

        public string Subject { get; set; }

        public JObject Object { get; set; }
    }
}
