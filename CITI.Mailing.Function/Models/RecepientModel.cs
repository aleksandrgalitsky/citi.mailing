﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class RecepientModel
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
