﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Function.Models
{
    public class MessageModel
    {
        public string Id { get; set; }

        public RecepientModel Recepient { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public DateTime Created { get; set; }

        public DateTime ScheduledAt { get; set; }

        public bool IsActive { get; set; }

        public bool IsSent { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public string Language { get; set; }
    }
}
