﻿using CITI.Mailing.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Mailing.Services.Contracts
{
    public interface IMailingService
    {
        Task<List<TemplateFull>> GetAll();

        Task<Template> Get(string id);

        Task<Template> Get(string app, string eventName);

        Task<TemplateBody> GetTemplateBody(string app, string eventName, string language);

        Task<List<TemplateBody>> GetTemplateBody(string app, string eventName);

        Task<string> GetBlob(string id, string language);

        Task<Template> Create(TemplateFull template);

        Task<Template> Create(string app, string name, string eventName, string language, string body);

        Task<TemplateFull> Update(TemplateFull template);

        Task<string> Delete(string id);

        Task NewQueue();

        Task CreateMessage(Message message, DateTime? schedule);

        Task Send(string queueMessageId);

        Task<List<Message>> GetMessages(string templateId, DateTime fromDate, DateTime toDate, int status);
    }
}
