﻿using CITI.Mailing.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CITI.Mailing.Services
{
    public static class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            Data.ContainerConfiguration.RegisterTypes(servicesCollection, config);
            Communication.ContainerConfiguration.RegisterTypes(servicesCollection, config);
            Blob.ContainerConfiguration.RegisterTypes(servicesCollection, config);

            servicesCollection.AddScoped<IMailingService, MailingService>();
        }
    }
}
