﻿using CITI.Mailing.Blob.Contracts;
using CITI.Mailing.Communication.Contracts;
using CITI.Mailing.Data.Contracts;
using CITI.Mailing.Data.Contracts.Repositories;
using CITI.Mailing.Model;
using CITI.Mailing.Services.Contracts;
using Newtonsoft.Json.Linq;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;
using System.Net.Http;
using Newtonsoft.Json;
using System.Linq;

namespace CITI.Mailing.Services
{
    public class MailingService: IMailingService
    {
        private IMailingDataContextManager _dataContextManager;
        private IAzureBlobClient _azureBlobClient;
        private IQueueManager _queueManager;
        private const string _renderedContainerName = "rendered";
        private const string _templateBodyContainerName = "templatesBody";

        public MailingService(IMailingDataContextManager dataContextManager, IAzureBlobClient azureBlobClient, IQueueManager queueManager)
        {
            _dataContextManager = dataContextManager;
            _azureBlobClient = azureBlobClient;
            _queueManager = queueManager;
        }

        public async Task<List<TemplateFull>> GetAll()
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();
            var iterator = await repoTemplate.GetQuery(x => x.IsActive);
            var templates = iterator.ToList();

            List<TemplateFull> result = new List<TemplateFull>();

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            foreach (var template in templates)
            {
                TemplateFull tpl = new TemplateFull();
                tpl.Id = template.Id;
                tpl.Name = template.Name;
                tpl.App = template.App;
                tpl.Event = template.Event;
                tpl.IsActive = template.IsActive;
                tpl.Items = new List<TemplateFullBody>();
                
                var iteratorTemplateBody = await repoTemplateBody.GetQuery(x => x.TemplateId == template.Id);
                var templateBody = iteratorTemplateBody.ToList();

                foreach(var body in templateBody)
                {
                    TemplateFullBody tplBody = new TemplateFullBody();
                    tplBody.Id = body.Id;
                    tplBody.Language = body.Language;

                    string bodyString = "";

                    if (await _azureBlobClient.FileExists(_templateBodyContainerName, body.Id))
                    {
                        using (Stream stream = await _azureBlobClient.DownloadFile(_templateBodyContainerName, body.Id))
                        {
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                bodyString = reader.ReadToEnd();
                            }
                        }
                    }

                    tplBody.Body = bodyString;

                    tpl.Items.Add(tplBody);
                }

                result.Add(tpl);
            }

            return result;
        }

        public async Task<Template> Get(string id)
        {
            var repo = _dataContextManager.CreateRepository<ITemplateRepository>();
            var data = await repo.Get(id);

            return data;
        }

        public async Task<Template> Get(string app, string eventName)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();
            var iterator = await repoTemplate.GetQuery(x => x.App == app && x.Event == eventName && x.IsActive);
            var template = iterator.ToList().FirstOrDefault();
            return template;
        }

        public async Task<TemplateBody> GetTemplateBody(string app, string eventName, string language)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();
            var template = await Get(app, eventName);

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            var iteratorTemplateBody = await repoTemplateBody.GetQuery(x => x.TemplateId == template.Id && x.Language == language);
            var templateBody = iteratorTemplateBody.ToList().FirstOrDefault();

            return templateBody;
        }

        public async Task<List<TemplateBody>> GetTemplateBody(string app, string eventName)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();
            var template = await Get(app, eventName);

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            var iteratorTemplateBody = await repoTemplateBody.GetQuery(x => x.TemplateId == template.Id);
            var templateBody = iteratorTemplateBody.ToList();

            return templateBody;
        }

        public async Task<string> GetBlob(string id, string language)
        {
            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            var iteratorTemplateBody = await repoTemplateBody.GetQuery(x => x.TemplateId == id && x.Language == language);
            var templateBody = iteratorTemplateBody.ToList().FirstOrDefault();

            string result = "";

            if(await _azureBlobClient.FileExists(_templateBodyContainerName, templateBody.Id))
            {
                using(Stream stream = await _azureBlobClient.DownloadFile(_templateBodyContainerName, templateBody.Id))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
            
            return result;
        }

        public async Task DeactivateTemplates(string app, string eventName)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();

            var iterator = await repoTemplate.GetQuery(x => x.App == app && x.Event == eventName);
            var templates = iterator.ToList();
            foreach (var item in templates)
            {
                item.IsActive = false;
                repoTemplate.AddOrUpdate(item);
            }

            await _dataContextManager.Save();
        }

        public async Task<Template> Create(TemplateFull template)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();

            await DeactivateTemplates(template.App, template.Event);

            Template newTemplate = new Template() { Id = Guid.NewGuid().ToString(), App = template.App, Name = template.Name, Event = template.Event, IsActive = true };
            repoTemplate.AddOrUpdate(newTemplate);
            await _dataContextManager.Save();

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();

            foreach (var body in template.Items)
            {
                TemplateBody newTemplateBody = new TemplateBody() { Id = Guid.NewGuid().ToString(), TemplateId = newTemplate.Id, Language = body.Language };
                repoTemplateBody.AddOrUpdate(newTemplateBody);
                await _dataContextManager.Save();

                using (MemoryStream stream = new MemoryStream())
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(body.Body);
                        writer.Flush();
                        stream.Position = 0;

                        await _azureBlobClient.UploadFile(_templateBodyContainerName, newTemplateBody.Id, stream);
                    }
                }
            }

            return newTemplate;
        }

        public async Task<Template> Create(string app, string name, string eventName, string language, string body)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();

            await DeactivateTemplates(app, eventName);

            Template newTemplate = new Template() { Id = Guid.NewGuid().ToString(), App = app, Name = name, Event = eventName, IsActive = true };
            repoTemplate.AddOrUpdate(newTemplate);

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            TemplateBody newTemplateBody = new TemplateBody() { Id = Guid.NewGuid().ToString(), TemplateId = newTemplate.Id, Language = language };
            repoTemplateBody.AddOrUpdate(newTemplateBody);

            await _dataContextManager.Save();

            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(body);
                    writer.Flush();
                    stream.Position = 0;

                    await _azureBlobClient.UploadFile(_templateBodyContainerName, newTemplateBody.Id, stream);
                }
            }

            return newTemplate;
        }

        public async Task<TemplateFull> Update(TemplateFull template)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();

            if(template.IsActive)
            {
                await DeactivateTemplates(template.App, template.Event);
            }
            
            Template updTemplate = await Get(template.Id);

            updTemplate.App = template.App;
            updTemplate.Event = template.Event;
            updTemplate.Name = template.Name;
            updTemplate.IsActive = template.IsActive;
            
            repoTemplate.AddOrUpdate(updTemplate);

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            foreach (var body in template.Items)
            {
                TemplateBody b = await repoTemplateBody.Get(body.Id);
                b.Language = body.Language;

                using (MemoryStream stream = new MemoryStream())
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(body.Body);
                        writer.Flush();
                        stream.Position = 0;

                        await _azureBlobClient.UploadFile(_templateBodyContainerName, body.Id, stream);
                    }
                }
            }

            await _dataContextManager.Save();

            return template;
        }

        public async Task<string> Delete(string id)
        {
            var repoTemplate = _dataContextManager.CreateRepository<ITemplateRepository>();
            Template template = await Get(id);

            var repoTemplateBody = _dataContextManager.CreateRepository<ITemplateBodyRepository>();
            var iteratorTemplateBody = await repoTemplateBody.GetQuery(x => x.TemplateId == template.Id);
            var templateBody = iteratorTemplateBody.ToList();

            foreach(var body in templateBody)
            {
                await _azureBlobClient.DeleteIfExists(_templateBodyContainerName, body.Id);

                repoTemplateBody.Delete(body);
            }

            repoTemplate.Delete(template);

            await _dataContextManager.Save();
            
            return "Ok";
        }

        public async Task NewQueue()
        {
            await _queueManager.EnusureQueueCreated();
            Communication.Models.Message msg = new Communication.Models.Message();
            msg.Id = Guid.NewGuid().ToString();

            DateTime schedule = new DateTime(2020, 1, 19, 4, 30, 0);

            await _queueManager.Enqueue(msg);
        }

        public async Task CreateMessage(Message message, DateTime? schedule = null)
        {
            var repo = _dataContextManager.CreateRepository<IMessageRepository>();
            repo.AddOrUpdate(message);
            await _dataContextManager.Save();

            string template = await GetBlob(message.TemplateId, message.Language);
            template = template.Replace("<!--TPL", "").Replace("TPL-->", "");
            dynamic renderModel = JValue.Parse(message.Body);

            var config = new TemplateServiceConfiguration();
            config.Language = Language.CSharp;
            config.EncodedStringFactory = new RawStringFactory();
            config.EncodedStringFactory = new HtmlEncodedStringFactory();
            config.CachingProvider = new DefaultCachingProvider();
            var service = RazorEngineService.Create(config);
            Engine.Razor = service;

            string result = Engine.Razor.RunCompile(template, message.TemplateId, null, renderModel as object);

            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(result);
                    writer.Flush();
                    stream.Position = 0;

                    await _azureBlobClient.UploadFile(_renderedContainerName, message.Id, stream);
                }
            }

            await _queueManager.EnusureQueueCreated();
            Communication.Models.Message msg = new Communication.Models.Message();
            msg.Id = message.Id;

            await _queueManager.Enqueue(msg, schedule);
        }

        public async Task Send(string queueMessageId)
        {
            string result = "";

            if (await _azureBlobClient.FileExists(_renderedContainerName, queueMessageId))
            {
                using (Stream stream = await _azureBlobClient.DownloadFile(_renderedContainerName, queueMessageId))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }

            var repo = _dataContextManager.CreateRepository<IMessageRepository>();
            Message messagedb = await repo.Get(queueMessageId);

            //repo.GetQuery(x => x.TemplateId)

            if (!string.IsNullOrEmpty(result) && messagedb != null)
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(messagedb.SenderName, messagedb.SenderEmail));
                message.To.Add(new MailboxAddress(messagedb.Recepient.Name, messagedb.Recepient.Email));
                message.Subject = messagedb.Subject;

                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = result;

                message.Body = bodyBuilder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("smtp.mandrillapp.com", 587, false);

                    client.Authenticate("CT Creative Technologies", "nkxbqgtcFNGYTONNgYH_DA");

                    client.Send(message);
                    client.Disconnect(true);
                }

                messagedb.IsSent = true;
                messagedb.IsActive = false;

                repo.AddOrUpdate(messagedb);
                await _dataContextManager.Save();
            }
        }

        public async Task<List<Message>> GetMessages(string templateId, DateTime fromDate, DateTime toDate, int status)
        {
            var repo = _dataContextManager.CreateRepository<IMessageRepository>();
            var iterator = await repo.GetQuery(x => x.TemplateId == templateId && x.ScheduledAt >= fromDate && x.ScheduledAt <= toDate);//templateId, fromDate, toDate, status);
            var messages = iterator.ToList();

            return messages;
        }
    }
}
