﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Services
{
    public class TemplateFullBody
    {
        public string Id { get; set; }

        public string Language { get; set; }

        public string Body { get; set; }
    }

    public class TemplateFull
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public bool IsActive { get; set; }

        [JsonProperty(PropertyName = "Items", Required = Required.Always)]
        [JsonConverter(typeof(SingleOrArrayConverter<TemplateFullBody>))]
        public List<TemplateFullBody> Items { get; set; }
    }
}
