using CITI.Mailing.Data.Repositories;
using CITI.Mailing.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CITI.Mailing.Data.Tests
{
    [TestClass]
    public class TemplateRepositoryTests
    {
        private MailingDataContextManager _dataContextManager;

        public TemplateRepositoryTests()
        {

            var services = new ServiceCollection();
            var config = new ConfigurationBuilder()
                .AddJsonFile("local.settings.json")
                .Build();

            services.AddLogging(options => options.AddDebug());

            ContainerConfiguration.RegisterTypes(services, config);
            var serviceProvider = services.BuildServiceProvider();
            _dataContextManager = serviceProvider.GetRequiredService<MailingDataContextManager>();
        }

        [TestMethod]
        public async Task EnsureDb()
        {
            await _dataContextManager.EnsureInitialized();
        }

        [TestMethod]
        public async Task AddTest()
        {
            var repo = _dataContextManager.CreateRepository<TemplateRepository>();
            var template = new Template { Id = Guid.NewGuid().ToString(), Name = "test1" };
            repo.AddOrUpdate(template);
            await _dataContextManager.Save();
        }

        [TestMethod]
        public async Task UpdateTest()
        {
            var repo = _dataContextManager.CreateRepository<TemplateRepository>();
            var template = await repo.Get("967a2b8f-38a0-4791-830d-dd6db5d3b4ed");
            var nameLen = template.Name;
            template.Name = template.Name + "1";
            repo.AddOrUpdate(template);
            await _dataContextManager.Save();

            template = await repo.Get("967a2b8f-38a0-4791-830d-dd6db5d3b4ed");
            Assert.AreEqual(nameLen + 1, template.Name);
        }

        [TestMethod]
        public async Task GetTest()
        {
            var repo = _dataContextManager.CreateRepository<TemplateRepository>();
            var template = await repo.Get("967a2b8f-38a0-4791-830d-dd6db5d3b4ed");
            Assert.IsNotNull(template);
        }

        [TestMethod]
        public async Task DeleteTest()
        {
            var repo = _dataContextManager.CreateRepository<TemplateRepository>();
            var count = (await repo.GetAll()).Count();
            var template = new Template { Id = Guid.NewGuid().ToString(), Name = "test1" };
            repo.AddOrUpdate(template);
            await _dataContextManager.Save();

            var count2 = (await repo.GetAll()).Count();
            Assert.AreEqual(count + 1, count2);

            repo.Delete(template);
            await _dataContextManager.Save();

            var count3 = (await repo.GetAll()).Count();
            Assert.AreEqual(count2 - 1, count3);
        }
    }
}
