﻿using CITI.Mailing.Model.Contracts;
using Newtonsoft.Json;
using System;

namespace CITI.Mailing.Model
{
    public class TemplateBody : IEntity
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public string TemplateId { get; set; }

        public string Language { get; set; }
    }
}
