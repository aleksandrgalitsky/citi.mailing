﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Model
{
    public class Recepient
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
