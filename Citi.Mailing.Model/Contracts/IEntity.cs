﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Mailing.Model.Contracts
{
    public interface IEntity
    {
        string Id { get; }

    }
}
