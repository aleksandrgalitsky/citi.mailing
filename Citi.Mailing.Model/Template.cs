﻿using CITI.Mailing.Model.Contracts;
using Newtonsoft.Json;
using System;

namespace CITI.Mailing.Model
{
    public class Template : IEntity
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public string Name { get; set; }

        public string App { get; set; }

        public string Event { get; set; }

        public bool IsActive { get; set; }
    }
}
