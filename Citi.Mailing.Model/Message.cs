﻿using CITI.Mailing.Model.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CITI.Mailing.Model
{
    public class Message : IEntity
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        public Recepient Recepient { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public DateTime Created { get; set; }

        public DateTime ScheduledAt { get; set; }

        public bool IsActive { get; set; }

        public bool IsSent { get; set; }

        public string Language { get; set; }

        public string TemplateId { get; set; }
    }
}
